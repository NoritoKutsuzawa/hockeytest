﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class BuildApplication
{
	[MenuItem("TestBuild/hogehoge")]
	public static void Build()
	{
		// ビルド対象シーンリスト
		string[] sceneList =
		{
			"./Assets/HockeyTestScene.unity"
		};

		string[] args = System.Environment.GetCommandLineArgs();

		var filePath =
			"C:/HockeyTest/"+
			"Test"+
			".apk";


		for (int i = 0; i < args.Length; i++)
		{
			switch (args[i])
			{
				case "/outpath":
					filePath = args[i + 1];
					break;
					
			}
		}
		


		// 実行
		var errorMessage = BuildPipeline.BuildPlayer(
			sceneList, //!< ビルド対象シーンリスト
			filePath, //!< 出力先
			BuildTarget.Android, //!< ビルド対象プラットフォーム
			BuildOptions.None //!< ビルドオプション
			);

//#if UNITY_EDITOR
//		// 結果出力
//		if (!string.IsNullOrEmpty(errorMessage))
//			Debug.LogError("[Error!] " + errorMessage);
//		else
//			Debug.Log("[Success!]");
//#endif
	}
}